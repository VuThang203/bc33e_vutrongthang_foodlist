export let renderDanhSach = (list) => {
  let contentHTML = "";
  list.forEach((item) => {
    let content = `<tr>
      <td>${item.id}</td>
      <td>${item.ten}</td>
      <td>${item.loai ? "Mặn" : "Chay"}</td>
      <td>${item.gia.toLocaleString()}</td>
      <td>${item.khuyenMai}</td>
      <td>0</td>
      <td>${item.tinhTrang ? "Còn" : "Hết"}</td>
      <td>
      <button data-toggle="modal" data-target="#exampleModal" onclick="suaMonAn(${
        item.id
      })" class="btn btn-warning">Sửa</button>
      <button onclick="xoaMonAn(${item.id}
      )" class="btn btn-primary">Xoá</button>
      </td>
      </tr>`;
    contentHTML += content;
  });
  document.getElementById("tbodyFood").innerHTML = contentHTML;
};

export let layThongTinTuForm = () => {
  let foodID = document.getElementById("foodID").value;
  let tenMon = document.getElementById("tenMon").value;
  let loai = document.getElementById("loai").value !== "loai1" ? true : false;
  let giaMon = document.getElementById("giaMon").value;
  let khuyenMai = document.getElementById("khuyenMai").value;
  let tinhTrang =
    document.getElementById("tinhTrang").value !== "0" ? true : false;
  let hinhMon = document.getElementById("hinhMon").value;
  let moTa = document.getElementById("moTa").value;

  return {
    foodID,
    tenMon,
    loai,
    giaMon,
    khuyenMai,
    tinhTrang,
    hinhMon,
    moTa,
  };
};

export let showThongTinLenForm = (list) => {
  document.getElementById("foodID").value = list.id;
  document.getElementById("tenMon").value = list.ten;
  document.getElementById("loai").value = list.loai ? "loai2" : "loai1";
  document.getElementById("giaMon").value = list.gia;
  document.getElementById("khuyenMai").value = list.khuyenMai;
  document.getElementById("tinhTrang").value = list.tinhTrang ? "1" : "0";
  document.getElementById("hinhMon").value = list.hinhAnh;
  document.getElementById("moTa").value = list.moTa;
};
