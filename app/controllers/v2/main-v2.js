import { renderDanhSach } from "./controller.v2.js";
import { layThongTinTuForm } from "./controller.v2.js";
import { monAnV2 } from "../../models/v2/listMonAn.v2.js";
import { showThongTinLenForm } from "./controller.v2.js";
const BARE_URL = "https://62f8b7a13eab3503d1da1abc.mockapi.io";
let batLoading = () => {
  document.getElementById("loading").style.display = "flex";
};
let tatLoading = () => {
  document.getElementById("loading").style.display = "none";
};

let renderTableService = () => {
  batLoading();
  axios({
    url: `${BARE_URL}/food`,
    method: "GET",
  })
    .then((res) => {
      tatLoading();
      console.log("res: ", res);
      renderDanhSach(res.data);
    })
    .catch((err) => {
      tatLoading();
      console.log("err: ", err);
    });
};
renderTableService();

document.getElementById("btnThemMon").addEventListener("click", function () {
  let data = layThongTinTuForm();
  let monAn = new monAnV2(
    data.tenMon,
    data.loai,
    data.giaMon,
    data.khuyenMai,
    data.tinhTrang,
    data.hinhMon,
    data.moTa,
    data.foodID
  );
  batLoading();
  axios({
    url: `${BARE_URL}/food`,
    method: "POST",
    data: monAn,
  })
    .then((res) => {
      tatLoading();
      $("#exampleModal").modal("hide");
      renderTableService();
    })
    .catch((err) => {
      tatLoading();
      console.log("err: ", err);
    });
});

function suaMonAn(id) {
  axios({
    url: `${BARE_URL}/food/${id}`,
    method: "GET",
  })
    .then((res) => {
      console.log("res: ", res.data);
      showThongTinLenForm(res.data);
    })
    .catch((err) => {
      console.log("err: ", err);
    });
}
window.suaMonAn = suaMonAn;

function xoaMonAn(id) {
  batLoading();
  axios({
    url: `${BARE_URL}/food/${id}`,
    method: "DELETE",
  })
    .then((res) => {
      tatLoading();
      renderTableService();
    })
    .catch((err) => {
      tatLoading();
      console.log("err: ", err);
    });
}
window.xoaMonAn = xoaMonAn;
