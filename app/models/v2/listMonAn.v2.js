export class monAnV2 {
  constructor(ten, loai, gia, khuyenMai, tinhTrang, hinhAnh, moTa, id) {
    this.ten = ten;
    this.loai = loai;
    this.gia = gia;
    this.khuyenMai = khuyenMai;
    this.tinhTrang = tinhTrang;
    this.hinhAnh = hinhAnh;
    this.moTa = moTa;
    this.id = id;
  }
}
